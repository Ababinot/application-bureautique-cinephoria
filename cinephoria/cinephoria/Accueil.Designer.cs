﻿using System.Windows.Forms;

namespace cinephoria
{
    partial class Accueil
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_salles = new System.Windows.Forms.Label();
            this.btn_declarer = new System.Windows.Forms.Button();
            this.lbl_h2_salles = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_deco = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_salles
            // 
            this.lbl_salles.AutoSize = true;
            this.lbl_salles.Font = new System.Drawing.Font("Microsoft PhagsPa", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_salles.Location = new System.Drawing.Point(27, 31);
            this.lbl_salles.Name = "lbl_salles";
            this.lbl_salles.Size = new System.Drawing.Size(112, 48);
            this.lbl_salles.TabIndex = 0;
            this.lbl_salles.Text = "Salles";
            // 
            // btn_declarer
            // 
            this.btn_declarer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.btn_declarer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_declarer.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_declarer.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_declarer.Location = new System.Drawing.Point(309, 43);
            this.btn_declarer.Name = "btn_declarer";
            this.btn_declarer.Size = new System.Drawing.Size(198, 35);
            this.btn_declarer.TabIndex = 1;
            this.btn_declarer.Text = "Déclarer un incident";
            this.btn_declarer.UseVisualStyleBackColor = false;
            this.btn_declarer.Click += new System.EventHandler(this.btn_declarer_Click);
            // 
            // lbl_h2_salles
            // 
            this.lbl_h2_salles.AutoSize = true;
            this.lbl_h2_salles.Font = new System.Drawing.Font("Microsoft PhagsPa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_h2_salles.Location = new System.Drawing.Point(31, 92);
            this.lbl_h2_salles.Name = "lbl_h2_salles";
            this.lbl_h2_salles.Size = new System.Drawing.Size(227, 23);
            this.lbl_h2_salles.TabIndex = 2;
            this.lbl_h2_salles.Text = "Visionner tout les incidents ";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(35, 153);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(864, 298);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btn_deco
            // 
            this.btn_deco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.btn_deco.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_deco.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_deco.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_deco.Location = new System.Drawing.Point(698, 43);
            this.btn_deco.Name = "btn_deco";
            this.btn_deco.Size = new System.Drawing.Size(160, 35);
            this.btn_deco.TabIndex = 4;
            this.btn_deco.Text = "déconnection";
            this.btn_deco.UseVisualStyleBackColor = false;
            this.btn_deco.Click += new System.EventHandler(this.btn_deco_Click);
            // 
            // Accueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(913, 489);
            this.Controls.Add(this.btn_deco);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lbl_h2_salles);
            this.Controls.Add(this.btn_declarer);
            this.Controls.Add(this.lbl_salles);
            this.Name = "Accueil";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Accueil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_salles;
        private System.Windows.Forms.Button btn_declarer;
        private System.Windows.Forms.Label lbl_h2_salles;
        private System.Windows.Forms.DataGridView dataGridView1;
        private Button btn_deco;
    }
}

