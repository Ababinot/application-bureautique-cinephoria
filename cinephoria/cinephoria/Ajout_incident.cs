﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace cinephoria
{
    public partial class Ajout_incident : Form
    {
        private List<Salle> salles; // Liste pour stocker les salles récupérées de l'API
        public event EventHandler IncidentAddedSuccessfully;

        public Ajout_incident()
        {
            InitializeComponent();
            LoadSallesAsync(); // Appeler la méthode de chargement des salles lors de l'initialisation du formulaire
        }

        private async Task LoadSallesAsync()
        {
            string apiUrl = "http://localhost:3001/api/salles"; // URL de votre API pour récupérer les salles

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.GetAsync(apiUrl);
                    response.EnsureSuccessStatusCode();

                    string responseBody = await response.Content.ReadAsStringAsync();
                    salles = JsonConvert.DeserializeObject<List<Salle>>(responseBody);

                    // Ajouter les numéros de salle à la ComboBox
                    comboBox1.DisplayMember = "NumSalle";
                    comboBox1.ValueMember = "IdSalle";
                    comboBox1.DataSource = salles;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show($"Erreur lors du chargement des salles : {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private async void btn_ajout_Click(object sender, EventArgs e)
        {
            await AjouterIncidentAsync();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Logique de gestion de l'événement SelectedIndexChanged de comboBox1
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            // Logique de gestion de l'événement TextChanged de richTextBox1
        }

        private void label1_Click(object sender, EventArgs e)
        {
            // Logique de gestion de l'événement Click du label1
        }


        private async Task AjouterIncidentAsync()
        {
            string apiUrl = "http://localhost:3001/api/incidentajout"; // URL de votre API pour ajouter un incident

            // Vérifier si un élément est sélectionné dans la ComboBox
            if (comboBox1.SelectedItem == null)
            {
                MessageBox.Show("Veuillez sélectionner une salle.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var incident = new
            {
                id_salle = (int)comboBox1.SelectedValue, // Récupérer l'IdSalle à partir du SelectedValue de la ComboBox
                commentaire = richTextBox1.Text // Récupérer le commentaire à partir du texte de richTextBox1
            };

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    var json = JsonConvert.SerializeObject(incident);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");

                    // Envoyer la requête POST
                    HttpResponseMessage response = await client.PostAsync(apiUrl, content);

                    if (!response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        MessageBox.Show($"Erreur lors de l'ajout de l'incident : {response.ReasonPhrase}\n\nDétails du serveur : {responseBody}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    MessageBox.Show("Incident ajouté avec succès.", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Nettoyer les champs après ajout
                    comboBox1.SelectedIndex = -1;
                    richTextBox1.Clear();
                    IncidentAddedSuccessfully?.Invoke(this, EventArgs.Empty);
                    this.Close();
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show($"Erreur lors de l'ajout de l'incident : {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }
    }

    public class Salle
    {
        [JsonProperty("id_salle")]
        public int IdSalle { get; set; }

        [JsonProperty("num_salle")]
        public int NumSalle { get; set; }

        // Ajoutez d'autres propriétés si nécessaire selon les données que vous récupérez
    }
}
