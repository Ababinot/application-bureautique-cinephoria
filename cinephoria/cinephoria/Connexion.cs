﻿using System;
using System.Net.Http;
using System.Text;
using System.Windows.Forms;

namespace cinephoria
{
    public partial class Connexion : Form
    {
        private const string apiUrl = "http://localhost:3001/api/login-bureautique"; // Remplacez par l'URL de votre API de connexion

        public Connexion()
        {
            InitializeComponent();
        }

        private async void btn_declarer_Click(object sender, EventArgs e)
        {
            string email = textBox1.Text.Trim();
            string password = textBox2.Text;

            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Veuillez saisir un email et un mot de passe.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    // Préparer les données d'authentification
                    var credentials = new { email = email, password = password };
                    var jsonContent = Newtonsoft.Json.JsonConvert.SerializeObject(credentials);
                    var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                    // Envoyer la requête POST à l'API
                    HttpResponseMessage response = await client.PostAsync(apiUrl, httpContent);

                    // Vérifier si la requête a réussi
                    if (response.IsSuccessStatusCode)
                    {
                        // Lecture de la réponse JSON
                        string responseBody = await response.Content.ReadAsStringAsync();
                        // Traitez ici la réponse selon les besoins
                        MessageBox.Show("Connexion réussie !", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        // Exemple de redirection vers une nouvelle forme après connexion réussie
                        Accueil accueil = new Accueil();
                        this.Hide();
                        accueil.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        // Lire la réponse d'erreur
                        string errorResponse = await response.Content.ReadAsStringAsync();
                        MessageBox.Show($"Échec de la connexion. Veuillez vérifier vos informations d'identification. Réponse de l'API : {errorResponse}", "Erreur de connexion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Erreur lors de la connexion : {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
