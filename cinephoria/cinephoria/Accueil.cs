﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace cinephoria
{
    public partial class Accueil : Form
    {
        private bool dataGridViewInitialized = false;
        private DataGridViewButtonColumn deleteButtonColumn; // Déclarer la colonne de bouton en tant que champ de classe

        public Accueil()
        {
            InitializeComponent();
        }

        private async void Accueil_Load(object sender, EventArgs e)
        {
            await LoadIncidentsAsync();
            dataGridViewInitialized = true; // Marquer le DataGridView comme initialisé après le premier chargement

            // Ajouter la colonne "Supprimer" seulement si elle n'existe pas déjà
            if (!dataGridView1.Columns.Contains("Supprimer"))
            {
                AddDeleteButtonColumn();
            }
        }

        private void btn_declarer_Click(object sender, EventArgs e)
        {
            Ajout_incident ajout_incident = new Ajout_incident();
            ajout_incident.IncidentAddedSuccessfully += Ajout_incident_IncidentAddedSuccessfully; // Abonnement à l'événement
            ajout_incident.Show();
        }

        private async Task LoadIncidentsAsync()
        {
            string apiUrl = "http://localhost:3001/api/incidents"; // Remplacez par l'URL de votre API

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.GetAsync(apiUrl);
                    response.EnsureSuccessStatusCode();

                    string responseBody = await response.Content.ReadAsStringAsync();
                    List<Incident> incidents = JsonConvert.DeserializeObject<List<Incident>>(responseBody);

                    // Exclure la colonne IdSalle
                    var incidentsToShow = incidents.Select(i => new
                    {
                        i.IdIncident,
                        i.Commentaire,
                        i.NumSalle
                    }).ToList();

                    // Définir la source de données du DataGridView
                    dataGridView1.DataSource = incidentsToShow;

                    // Ajuster la largeur de la colonne "Commentaire"
                    dataGridView1.Columns["Commentaire"].Width = 300;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show($"Erreur lors de l'appel à l'API: {ex.Message}");
                }
            }
        }

        private async void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex] == deleteButtonColumn && e.RowIndex >= 0)
            {
                var incidentIdToDelete = (int)dataGridView1.Rows[e.RowIndex].Cells["IdIncident"].Value;

                DialogResult result = MessageBox.Show("Êtes-vous sûr de vouloir supprimer cet incident ?", "Confirmation de suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    await DeleteIncidentAsync(incidentIdToDelete);
                    await LoadIncidentsAsync();
                }
            }
        }

        private async void Ajout_incident_IncidentAddedSuccessfully(object sender, EventArgs e)
        {
            await LoadIncidentsAsync();
        }

        private async Task DeleteIncidentAsync(int incidentId)
        {
            string apiUrl = $"http://localhost:3001/api/incident/{incidentId}";

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.DeleteAsync(apiUrl);
                    response.EnsureSuccessStatusCode();
                    MessageBox.Show("Incident supprimé avec succès.", "Suppression réussie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show($"Erreur lors de la suppression de l'incident : {ex.Message}", "Erreur de suppression", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void AddDeleteButtonColumn()
        {
            deleteButtonColumn = new DataGridViewButtonColumn();
            deleteButtonColumn.HeaderText = "Supprimer";
            deleteButtonColumn.Text = "Supprimer";
            deleteButtonColumn.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(deleteButtonColumn);
        }

        private void btn_deco_Click(object sender, EventArgs e)
        {
            // Fermer l'Accueil et ouvrir le formulaire de Connexion
            this.Hide(); // Masquer le formulaire actuel
            Connexion connexionForm = new Connexion();
            connexionForm.ShowDialog(); // Afficher le formulaire de connexion
            this.Close(); // Fermer le formulaire Accueil
        }
    }

    public class Incident
    {
        [JsonProperty("id_incident")]
        public int IdIncident { get; set; }

        [JsonProperty("commentaire")]
        public string Commentaire { get; set; }

        [JsonProperty("num_salle")]
        public int NumSalle { get; set; }

        [JsonProperty("id_salle")]
        public int IdSalle { get; set; }
    }
}
